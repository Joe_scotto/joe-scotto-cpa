<?php

class Mail {
    /**
     * Sends an email to contact@joescottocpa.com
     * @param  mixed $data $_POST Data
     * @param  string $url Link to post back to
     * @return string Success Message
     */
    public static function send ($data) {
        //Send Email
        mail("contact@joescottocpa.com", $data['subject'], "From: " . $data['name'] . "\n"  . "Email: " . $data['email'] . "\n"  . "Phone: " . $data['phone'] . "\n\n" . $data['message'], "From: New Message <contact@coilerz.com>");

        //Set session for success modal
        $_SESSION['success'] = 1;
    }
}