<?php require 'core/init.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- Bootstrap Mobile Optimization -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- Meta Tags -->
    <meta name="description" content="Joseph Scotto is a CPA working out of Chittenango NY and licensed in New York and Virginia. Joe specializes in IRS Audit Defense, Tax Preparation, and consulting. " />
    <meta name="google-site-verification" content="sZl2PeGePBO6vv8KwGY7FH8pZKnkUvg87OslTuPFESE" />
    <title>Joseph M. Scotto, C.P.A.</title>
    <!-- Favicon -->
    <link rel="icon" href="images/favicon.png">
    <!-- Bootstrap CDN CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- Main Stylesheet -->
    <link rel="stylesheet" href="css/style.css">
        <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-81131634-1', 'auto');
      ga('send', 'pageview');

    </script>
</head>

<body>
    <div class="banner_message">
        <div class="container">
            <div class="left">$99 Tax Returns</div>
            <div class="right contact">Contact Me</div>
        </div>
    </div>

    <!-- Success Modal -->
    <div class="modal fade success_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="padding:10px 50px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4>Success</h4>
                </div>
                <div class="modal-body" style="padding:15px 50px;">
                    Message was sent successfully! Please allow up to 7 days for a response.
                </div>
            </div>
        </div>
    </div>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="images/logo.png"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="about"><a href="#about">About</a></li>
                    <li class="services"><a href="#services">Services</a></li>
                    <li class="contact"><a href="#contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Hero -->
    <div id="hero_container">

        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="images/portrait.jpg" class="img-responsive img-circle">
                    <h1>Professional. Cost Effective. Quick.</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- About Section -->
    <div id="about_container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1>About</h1>
                    <hr>
                    <p>I am a licensed CPA in NY and Virginia and have over 18 years of experience in public accounting and private Industry specializing in taxation. I graduated with honors from Old Dominion University with a Bachelor's of Science degree in Business Administration. My career began at a large regional firm in Virginia where I later accepted a position with a big four national firm. After many years in public accounting, I accepted a leadership role with a multi-billion dollar multinational company and led the compliance and planning efforts for that company. I relocated to Chittenango, NY and currently handle all tax matters for a billion dollar company based in Syracuse, New York. Throughout my career, I have assisted individuals and companies with all types of tax issues and have the knowledge necessary to assist you.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Specialties Section -->
    <div id="specialties_container">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <img src="images/shield.png" class="img-responsive">
                    <h1 class="text-center">IRS Audit Defense</h1>
                    <p class="text-center irs">If you have been contacted by the IRS, I can help you resolve your tax issues. I have many years of experience dealing with the IRS with favorable outcomes. No issue is too small.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <img src="images/clipboard.png" class="img-responsive">
                    <h1 class="text-center">Tax Return Preparation</h1>
                    <p class="text-center">If you need a new tax advisor to assist you with your annual and quarterly tax compliance, contact me, and I can provide you with a free fee quote. </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Services Section -->
    <div id="services_container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1>Services</h1>
                    <hr>
                    <ul>
                        <li>Federal and State tax return preparation for businesses and individuals</li>
                        <li>Tax planning</li>
                        <li>Prior year tax return review</li>
                        <li>Multi state nexus analysis</li>
                        <li>Unitary combined reporting analysis</li>
                        <li>Tax research and accounting methods review</li>
                        <li>Complex tax provision preparation and review</li>
                        <li>Quarterly compliance, estimated taxes and extensions</li>
                        <li>Tax Audit Defense – Federal and State</li>
                        <li>Tax notices</li>
                        <li>Tax depreciation expert</li>
                        <li>Contract review for tax compliance</li>
                        <li>Tax projections</li>
                        <li>Sales and use tax guidance</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- Contact Section -->
    <div id="contact_container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1>Contact</h1>
                    <ul>
                        <li>4011 Brick Kiln Drive, Chittenango, NY, 13037</li>
                        <li>954 - 425 - 2320</li>
                        <li>contact@joescottocpa.com</li>
                    </ul>                   
                    <hr>
                </div>
                <form method="post" action="">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Name" name="name" required>
                        </div>  
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="Email" name="email" required>
                        </div> 
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="phone" class="form-control" id="phone" placeholder="Phone" name="phone" required>
                        </div> 
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" id="subject" placeholder="Subject" name="subject" required>
                        </div> 
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea class="form-control" id="message" name="message" required></textarea>
                        </div> 
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <input type="submit" name="submit" value="Send Message">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Footer Section -->
    <div id="footer_container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1>Copyright 2016, Joe Scotto</h1>
                    <h3>Site Created By: <a href="https://twitter.com/joe_scotto">Joe Scotto</a></h3>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery CDN -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <!-- Bootstrap CDN JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script type="text/javascript">

    //About Scroll
    $(".about").click(function() {
        $('html, body').animate({
            scrollTop: $("#about_container").offset().top
        }, 500);
    });

    //About Scroll
    $(".contact").click(function() {
        $('html, body').animate({
            scrollTop: $("#contact_container").offset().top
        }, 500);
    });

    //Services Scroll
    $(".services").click(function() {
        $('html, body').animate({
            scrollTop: $("#specialties_container").offset().top
        }, 500);
    });

    </script>
    <!-- Show success modal if there is an error -->
    <?php if (isset($_SESSION['success'])) {?>
    <script>
    $(document).ready(function (e) {
        $(".success_modal").modal("show");
    });
    </script>
    <?php } ?>
</body>
</html>