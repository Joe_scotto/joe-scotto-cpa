<?php

//Start Sessions
session_start();

//Turn Off Errors
error_reporting(0);

//Unset Session
unset($_SESSION['success']);

//Autoload Classes
spl_autoload_register(function ($class) {
    require 'classes/' . $class . '.php';
});

//Send Email
if (isset($_POST['submit'])) {
    if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['phone']) && !empty($_POST['subject']) &&    !empty($_POST['message'])) {
        Mail::send($_POST);
    }
}